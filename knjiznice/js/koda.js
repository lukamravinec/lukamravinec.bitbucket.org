
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var ehrJoza = "e7f45a5a-6d46-4e3a-b998-852303839d16";
var ehrMicka = "aa808e86-e0cc-4fc9-a739-57f96df35a5d";
var ehrDarth = "9933944d-5280-46e4-a35c-af0ef035727c";

function nastaviPodatke(osebaEhr) {

	sessionId = getSessionId();

	var ehrId = osebaEhr;

	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
			$("#preberiSporocilo").html("<span class='obvestilo label " +
      "label-success fade-in'>Bolnik '" + party.firstNames + " " +
      party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
      "'.</span>");
		},
		error: function(err) {
			alert("error");
		}
	});
}


function preberiPodatkeUporabnika(ehrId)
{
	var sessionId = getSessionId();
	
	$.ajaxSetup({
	    headers: {
	        "Ehr-Session": sessionId
	    }
	});

	var searchData = [
	    {key: "ehrId", value: ehrId}
	];

	$.ajax({
	    url: baseUrl + "/demographics/party/query",
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(searchData),
	    success: function (res) {
	    	if(res != null)
		        for (i in res.parties) {
		            var party = res.parties[i];
		            $("#ime").text(party.firstNames);
		            $("#priimek").text(party.lastNames);
		            $("#ehrId").text(ehrId); 
		        }
		    else
		    {
		    	console.log("EhrId ni veljaven");
		    }
	    },
	    error: function(err){
	    	alert("err");
	    }
	});
}

function zamenjajEhr() {
	
	$("#temp").html("");
	$("#tabela_krvniTlak").html("<tr>\
									<th><span></span> Datum mertive:</th>\
									<th><span></span> Sistolični tlak: [mm/Hg]:</th>\
									<th><span></span> Diastolični tlak: [mm/Hg]:</th>\
								</tr>");
	
	var i = event.target.id;
	
	switch(i) {
		case '0':
			preberiPodatkeZaVitalneZnake(ehrJoza);
			break;
		case '1':
			preberiPodatkeZaVitalneZnake(ehrMicka);
			break;
		case '2':
			preberiPodatkeZaVitalneZnake(ehrDarth);
			break;
		case '3':
			generirajPodatke(1,function(d){alert(d);});
			generirajPodatke(2,function(d){alert(d);});
			generirajPodatke(3,function(d){alert(d);});
			break;
		case 'vnosEhrId':
			var e = $("#ehrIdVsebina").val();
			preberiPodatkeZaVitalneZnake(e);
			break;
		default:
			alert("napaka69");
			break;
	}
}


function preberiPodatkeZaVitalneZnake(ehrId) {
	var sessionId = getSessionId();	
	$.ajaxSetup({
	    headers: {
	        "Ehr-Session": sessionId
	    }
	});
	
	preberiPodatkeUporabnika(ehrId);

	var aql = "SELECT " +
    "a/content[openEHR-EHR-OBSERVATION.body_temperature.v1]/data[at0002]/events[at0003]/data[at0001]/items[at0004] as telTempVr, "+
    "a/content[openEHR-EHR-OBSERVATION.body_temperature.v1]/data[at0002]/events[at0003]/time as telTempCas, "+
    "a/content[openEHR-EHR-OBSERVATION.blood_pressure.v1]/data[at0001]/events[at0006]/data[at0003] as telKrvniTlak, "+
    "a/content[openEHR-EHR-OBSERVATION.blood_pressure.v1]/data[at0001]/events[at0006]/time as telKrvniTlakCas, " +
    "a/content[openEHR-EHR-OBSERVATION.height.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0004] as telVisinaVr, "+
    "a/content[openEHR-EHR-OBSERVATION.height.v1]/data[at0001]/events[at0002]/time as telVisinaCas, "+
    "a/content[openEHR-EHR-OBSERVATION.body_weight.v1]/data[at0002]/events[at0003]/data[at0001]/items[at0004] as telTezaVr, "+
    "a/content[openEHR-EHR-OBSERVATION.body_weight.v1]/data[at0002]/events[at0003]/time as telTezaCas, "+
    "a/content[openEHR-EHR-OBSERVATION.indirect_oximetry.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0006] as nasicKrviSKisikomVr, "+
    "a/content[openEHR-EHR-OBSERVATION.indirect_oximetry.v1]/data[at0001]/events[at0002]/time as nasicKrviSKisikomCas, "+
    "a/territory/code_string as territory_code_string "+
	"from EHR e "+
	"contains COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] "+
	"where "+
	    "a/name/value='Vital Signs' and "+
	    "e/ehr_id/value='"+ ehrId +"' "+
	"offset 0 limit 100";	

	$.ajax({
	    url: baseUrl + "/query?" + $.param({"aql": aql}),
	    type: 'GET',
	    success: function (res) {
	        if(res != null)
	        {
	        	var rows = res.resultSet;
		        izlusciPodatkeVitalnihZnakov(rows);

	        } else
	        {
	        	console.log('AQL pozvedba za dan ehrId ni obrodila sadov - rezultat je prazna množica!');
	        }
	    },
	    error: function(err) {
			console.log(JSON.parse(err.responseText).userMessage);
		}
	});
}


function izlusciPodatkeVitalnihZnakov(rows)
{
	var podatkiTelTemp = new Array();
	var podatkiTelTlak = new Array();

	for(var key in rows)
	{	
		
		var casMeritveTemp = rows[key].telTempCas.value;
		var vrednostMeritveTemp = rows[key].telTempVr.value.magnitude;
		var vrednostMeritveSisTlaka = rows[key].telKrvniTlak.items[0].value.magnitude; // systol
		var vrednostMeritveDiasTlaka = rows[key].telKrvniTlak.items[1].value.magnitude; // diastol
		var casMeritveKrvnegaTlaka = rows[key].telKrvniTlakCas.value;

		podatkiTelTemp.push({"cas": new Date(casMeritveTemp), "temp": vrednostMeritveTemp});
		podatkiTelTlak.push({"cas": new Date(casMeritveKrvnegaTlaka), "sistolicni": vrednostMeritveSisTlaka, "diastolicni": vrednostMeritveDiasTlaka});
	}

	podatkiTelTemp.sort(function(a,b){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});

	podatkiTelTlak.sort(function(b,a){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});
	telesnaTemperaturaIzpis(podatkiTelTemp);
	krvniTlakIzpis(podatkiTelTlak);
	
}

function telesnaTemperaturaIzpis(podatkiTelTemp)
{
	// Graf za predstavitev telesne temperature, višine in krvnega pritiska.

	var margin = {
		top: 20,
		right: 20, 
		bottom: 86,
		left: 80
	},
		width = 600 - margin.left - margin.right,
		height = width - margin.top - margin.bottom;

	//var x = d3.scale.ordinal()
	//	.rangeRoundBands([0, width], .06);
	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .06);
		
	var y = d3.scale.linear()
		.rangeRound([height, 0]);

	var color = d3.scale.ordinal()
		.range(["#308fef", "5fa9f3", "1176db"]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom")
		.tickFormat(d3.time.format("%Y-%m-%d"));

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")
		.ticks(10);

	var svg = d3.select("#temp").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");



	x.domain(podatkiTelTemp.map(function(d){
		return d.cas;
	}));	

	y.domain([0, d3.max(podatkiTelTemp, function(d){
		return d.temp;
	})+3]);

	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis)
	.selectAll("text")
		.style("text-anchor", "end")
		.attr("dx", "0.60em")
		.attr("dy", "0.80em")
		.attr("transform", "rotate(-45)" )
	.append("text")
		.attr("dy", "-.82em")
      	.style("text-anchor", "end");

	svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
	.append("text")
      	.attr("dy", "-.82em")
      	.style("text-anchor", "end")
      	.text("Temperatura (°C)");

    var temp_info = $("#temp_info");

    svg.selectAll("bar")
    	.data(podatkiTelTemp)
    .enter().append("rect")
    	.style("fill", "steelblue")
    	.style("opacity", .8)
    	.attr("x", function(d) { return x(d.cas); })
    	.attr("width", x.rangeBand())
    	.attr("y", function(d) {return y(d.temp); })
    	.attr("height", function(d) { return height - y(d.temp); })

    	.on("mouseover", function(d){
    		temp_info.css("visibility", "visible");
    		temp_info.html("<small style='color:green; font-style:italic;'>" + d.cas + " </small>&nbsp;&nbsp;&nbsp;&nbsp;" + d.temp + "°C");
    		$(this).css("fill", "green");


    	})
    	.on("mouseout", function(d){
    		temp_info.css("visibility", "hidden");
    		$(this).css("fill", "steelblue");
    	});
    

}

function krvniTlakIzpis(podatkiTelTlak)
{
	for(key in podatkiTelTlak)
	{
		$("#tabela_krvniTlak tr:last").after("<tr><td>" + podatkiTelTlak[key]["cas"] + "</td><td>" + podatkiTelTlak[key]["sistolicni"] + "</td><td>" + podatkiTelTlak[key]["diastolicni"] + "</td></tr>");

	}
}



/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
    //1-3 imena; 4 generiraj vse 3 in alert
  var  sessionId = getSessionId();
  //var ehrId = "";
   // var tmp = "";
  if (stPacienta == 1) {
      
      $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Joža",
		            lastNames: "Hišna",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	dodajMeritveVitalnihZnakov(ehrId);
		                    callback(ehrId);
		                   
		                }
		            },
		            error: function(err) {
		            	alert("napaka");
		            }
		        });
		    }
		});
  }
  
  if (stPacienta == 2) {
       $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Micka",
		            lastNames: "Sosedova",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	dodajMeritveVitalnihZnakov(ehrId);
		                    callback(ehrId);
		                   
		                }
		            },
		            error: function(err) {
		            	alert("napaka");
		            }
		        });
		    }
		});
  }
  
  if (stPacienta == 3) {
       $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Darth",
		            lastNames: "Vader",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	dodajMeritveVitalnihZnakov(ehrId);
		                    callback(ehrId);
		                   
		                }
		            },
		            error: function(err) {
		            	alert("napaka");
		            }
		        });
		    }
		});
  }
}


function dodajMeritveVitalnihZnakov(ehrid) {
	var sessionId = getSessionId();

	var ehrId = ehrid;
	var telesnaTemperatura = Math.floor((Math.random() * 5) + 36);
	var sistolicniKrvniTlak = Math.floor((Math.random() * 30) + 100);
	var diastolicniKrvniTlak = Math.floor((Math.random() * 30) + 80);

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		       
		    },
		    error: function(err) {
		    	alert("erroe69");
		    }
		});
	
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function prikazPodrobnosti() {

     $.ajax({
        url : event.target.id + ".txt",
        dataType: "text",
        success : function (data) {
            $("#podrobnostiZdravila").html(data);
        }
    });
}

function izpisiZdravila() {
	 $.getJSON("zdravila.json", function(json) {
        for (var i = 0; i < json.z1.length; i++) {
             $("#glavna_tabela").append('\
                <tr>\
  					<th class="small" onclick="prikazPodrobnosti()" id="' + i +'">' + json.z1[i].ime + '</th>\
  			    </tr>\
            ');
        }
    });
}

$(document).ready(function(){
    
    izpisiZdravila();
    
   preberiPodatkeZaVitalneZnake(ehrJoza);
});


function kreirajEHRzaBolnika(ime, priimek, callback) {
	sessionId = getSessionId();

	if (!ime || !priimek  || ime.trim().length == 0 ){
      alert("napaka1");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    callback(ehrId);
		                   
		                }
		            },
		            error: function(err) {
		            	alert("napaka");
		            }
		        });
		    }
		});
	}
}
